import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vuelidate from "vuelidate";
import vuetify from "./plugins/vuetify";
import vmodal from "vue-js-modal";
import VueYouTubeEmbed from "vue-youtube-embed";
import VueCookies from "vue-cookies";
import VueSocialSharing from "vue-social-sharing";
import VueHtmlToPaper from "vue-html-to-paper";

const options = {
  name: "_blank",
  specs: ["fullscreen=yes", "titlebar=yes", "scrollbars=yes"],
  styles: [
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
    "https://unpkg.com/kidlat-css/css/kidlat.css"
  ]
};

Vue.use(VueHtmlToPaper, options);

Vue.use(VueSocialSharing);
Vue.use(VueCookies);
Vue.use(VueYouTubeEmbed);
Vue.use(Vuelidate);
Vue.use(vmodal, { dialog: true });
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
