export default {
  name: "company",
  data() {
    return {
      company: false,
      contact: ""
    };
  },
  methods: {
    getContact(v) {
      if (v !== null) {
        if (v.CEO !== null) {
          this.company = true;
          this.contact = v.Companyname;
        } else {
          if (v.Pavarde == null) {
            this.contact = v.Vardas;
          } else {
            this.contact = v.Vardas + " " + v.Pavarde;
          }
        }
      }
    }
  }
};
