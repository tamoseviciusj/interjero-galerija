import { mapGetters, mapActions } from "vuex";
export default {
  name: "loggedin",
  computed: mapGetters(["getUser"]),
  data() {
    return {
      user: false
    };
  },
  methods: {
    ...mapActions(["disconnect"]),
    disconnectUser(e) {
      e.preventDefault();
      this.disconnect();
    }
  },
  created() {
    this.user = this.getUser == null ? false : this.getUser;
  }
};
