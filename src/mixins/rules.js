export default {
  name: "rules",

  data() {
    return {
      inputRules: [v => v.length >= 3 || "Minimum 3 simboliai"],
      emailRules: [
        v => !!v || "Reikalingas elektroninis pastas",
        v => /.+@.+\..+/.test(v) || "Neteisingas suvestas elektroninis pastas"
      ],
      passwordRules: [v => v.length >= 6 || "Minimum 6 simboliai"],
      match: [
        () => this.passwordre === this.password || "Slaptazodziai nesutampa"
      ],
      empty: [v => v.length >= 1 || "Pamirstas laukelis"]
    };
  }
};
