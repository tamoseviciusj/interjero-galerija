import Vue from "vue";
import VueRouter from "vue-router";
import Register from "../views/Register.vue";
import Login from "../views/Login.vue";
import Skelbimas from "../views/Skelbimas.vue";
import AdUpload from "../views/AdUpload.vue";
import UserPage from "../views/UserPage.vue";
import MainPage from "../views/MainPage.vue";
import PersonalAds from "../views/PersonalAds.vue";
import SearchPage from "../views/SearchPage.vue";
import Favorites from "../views/Favorites.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/isimintini",
    name: "Favorites",
    component: Favorites,
  },
  {
    path: "/asmeniniai",
    name: "personalads",
    component: PersonalAds,
  },
  {
    path: "/paieska",
    name: "searchPage",
    component: SearchPage,
  },
  {
    path: "/vartotojas",
    name: "UserPage",
    component: UserPage,
  },
  {
    path: "/pagrindinis",
    name: "Main",
    component: MainPage,
  },
  {
    path: "/idetiskelbima",
    name: "AdUpload",
    component: AdUpload,
  },

  {
    path: "/Register",
    name: "Register",
    component: Register,
  },
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/Skelbimas/:id",
    name: "Skelbimas",
    component: Skelbimas,
    props: true,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
