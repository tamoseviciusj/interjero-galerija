// eslint-disable
import axios from "axios";
//import jsonp from "jsonp";
import VueCookies from "vue-cookies";
import Vue from "vue";
import router from "../router";
import { vuexfireMutations } from "vuexfire";

const URL = "http://185.34.52.113:1337";
const EMAIL = "http://185.34.52.113";

Vue.use(VueCookies);
const state = {
  error: "",
  user: Vue.$cookies.get("user"),
  token: Vue.$cookies.get("token"),
  post: {},
  comments: {},
  myPosts: [],
  filteredPosts: [],
  URL: "http://185.34.52.113:1337",
  EMAIL: "http://185.34.52.113",
};

const getters = {
  getError: (state) => state.error,
  getUser: (state) => state.user,
  getToken: (state) => state.token,
  getPost: (state) => state.post,
  getComment: (state) => state.comments,
  getFullUser: (state) => {
    if (state.user.Companyname) {
      return state.user.Companyname;
    } else {
      return state.user.Vardas + " " + state.user.Pavarde;
    }
  },
};

const mutations = {
  ...vuexfireMutations,
  register: (state, error) => {
    state.error = error;
  },
  login: (state, error) => {
    state.error = error;
  },
  noError: (state, msg) => (state.error = msg),
  disconnect: (state) => (state.user = ""),
  post: () => "",
  singlePost: (state, data) => (state.post = data),
  getComments: (state, comments) => (state.comments = comments),
  comment: () => "",
  emailSend: () => "",
  postImage: () => "",
  confirmPost: () => "",
  setMyPosts: (state, posts) => (state.myPosts = posts),
  setAllPosts: (state, posts) => (state.filteredPosts = posts),
};

const actions = {
  getFilteredPosts: async ({ commit }, query) => {
    let stringAttachment = "";
    if (query == undefined) {
      stringAttachment += "";
    } else {
      stringAttachment += `?name_contains=${query}`;
    }

    await axios.get(`${URL}/posts` + stringAttachment).then(({ data }) => {
      commit("setAllPosts", data);
    });
  },
  removePost: async ({ commit }, id) => {
    await axios.delete(`${URL}/posts/${id}`, {
      headers: {
        Authorization: `Bearer ${state.token}`,
      },
    });
  },
  addViews: async ({ commit }, props) => {
    const addUpViews = props.views + 1;
    await axios.put(`${state.URL}/posts/${props.id}`, {
      amountViews: addUpViews,
    });
  },
  getMyPosts: async ({ commit }) => {
    await axios
      .get(`${URL}/posts?user.email=${state.user.email}`)
      .then(({ data }) => {
        commit("setMyPosts", data);
        console.log(state.myPosts);
      });
  },
  async comment({ commit }, v) {
    console.log(state.token);
    await axios
      .post(
        URL + "/comments",
        { Comment: v.Comment, Name: v.Name, post: { id: v.id } },
        {
          headers: {
            Authorization: `Bearer ${state.token}`,
          },
        }
      )
      .then((response) => {
        commit("comment");
        return response.data;
      });
  },
  async getComments({ commit }, v) {
    await axios.get(URL + `/comments?post.id=${v.id}`).then((response) => {
      commit("getComments", response.data);
    });
  },
  async getPosts({ commit }, v) {
    if (v.id) {
      await axios
        .get(URL + `/posts/${v.id}`)
        .then((response) => {
          commit("singlePost", response.data);
          return response.data;
        })
        .catch((err) => console.log(err));
    }
  },
  async emailSend({ commit }, v) {
    await axios
      .post(EMAIL, {
        emailTo: v.emailTo,
        message: v.message,
        subject: v.subject,
        name: v.name,
      })
      .then(() => {
        commit("emailSend");
      });
  },
  confirmPost({ commit }, v) {
    axios.put(URL + "/posts/" + v.id, {
      confirmed: true,
    });
    commit("confirmPost");
  },
  post({ commit }, v) {
    axios
      .post(
        URL + "/posts",
        {
          name: v.name,
          category: v.category,
          description: v.description,
          price: v.price,
          video: v.video,
          user: {
            id: state.user.id,
          },
        },
        {
          headers: {
            Authorization: `Bearer ${state.token}`,
          },
        }
      )
      .then((response) => {
        console.log(v.list);
        for (let i = 0; v.list.length > i; i++) {
          console.log(v.list[i]);
          const formData = new FormData();
          formData.append("files", v.list[i]);
          formData.append("ref", "Posts");
          formData.append("refId", response.data.id);
          formData.append("field", "images");
          axios.post(URL + "/upload", formData);

          commit("postImage");
        }
        commit("post");
        setTimeout(() => {
          router.push("/skelbimas/" + response.data.id);
        }, 2000);
      });
  },

  postEdit({ commit }, v) {
    axios
      .put(
        URL + "/posts/" + v.id,
        {
          name: v.name,
          category: v.category,
          description: v.description,
          price: v.price,
          video: v.video,
          images: [],
        },
        {
          headers: {
            Authorization: `Bearer ${state.token}`,
          },
        }
      )
      .then((response) => {
        console.log(v.list);
        for (let i = 0; v.list.length > i; i++) {
          console.log(v.list[i]);
          const formData = new FormData();
          formData.append("files", v.list[i]);
          formData.append("ref", "Posts");
          formData.append("refId", response.data.id);
          formData.append("field", "images");
          axios.post(URL + "/upload", formData);

          commit("postImage");
        }
        commit("post");
        setTimeout(() => {
          router.push("/skelbimas/" + response.data.id);
        }, 2000);
      });
  },
  async postImage({ commit }, v) {
    for (let i = 0; v.list.list <= i; i++) {
      console.log(v.list[i]);
      const formData = new FormData();
      formData.append("files", v.list[i]);
      formData.append("ref", "Posts");
      formData.append("refId", v.id);
      formData.append("field", "images");
      axios.post(URL + "/upload", formData);

      commit("postImage");
    }
  },
  //DISCONNECT
  disconnect({ commit }) {
    Vue.$cookies.remove("user");
    Vue.$cookies.remove("token");
    commit("disconnect");
    location.reload();
  },
  //DISCONNECT

  register({ commit }, v) {
    axios
      .post(URL + "/auth/local/register", {
        username: v.email,
        email: v.email,
        password: v.password,
        Vardas: v.Vardas,
        Pavarde: v.Pavarde,
        Phone: v.Phone,
        Companyname: v.Companyname,
        Companycode: v.Companycode,
        Address: v.Address,
        City: v.City,
        CEO: v.CEO,
      })
      .then((response) => {
        console.log(response);
        commit("noError", "");
      })
      .catch(() => {
        commit(
          "register",
          "Ivyko klaida, toks elektroninis pastas jau naudojamas."
        );
      });
  },
  login({ commit }, v) {
    // set default config
    axios
      .post(URL + "/auth/local", {
        identifier: v.email,
        password: v.password,
      })
      .then((response) => {
        // Handle success.
        delete response.data.user.posts;

        Vue.$cookies.set("token", response.data.jwt, "12h");
        Vue.$cookies.set("user", response.data.user, "12h");

        console.log(Vue.$cookies.get("user"));

        commit("noError", "");

        location.reload();
      })
      .catch(() => {
        // Handle error.
        commit(
          "login",
          "Ivyko klaida, neteisingai ivestas elektroninis pastas, arba slaptazodis"
        );
      });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
